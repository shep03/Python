#!/usr/bin/env python3

# https://dns.google.com/resolve?name=r3d3mpt10n.com&type=A'

import json, requests 
import argparse

parser = argparse.ArgumentParser(description='Lookup DNS records over HTTPS from Google')
parser.add_argument(
        '-n', 
        '--name', 
        dest='NAME', 
        required=True, 
        help='The Hostname you want to lookup. eg redhat.com'
        )
parser.add_argument(
        '-t', 
        '--type', 
        dest='TYPE', 
        required=False, 
        help='The type of record that you are looking for. A, MX, AAAA, TXT'
        )
args=parser.parse_args()


if args.NAME:
   NAME = args.NAME
if args.TYPE:
   TYPE = args.TYPE
else:
    TYPE = "A"

#TYPE = "A"
#NAME = "redhat.com"
query = "?name=%s&type=%s"  % ( NAME, TYPE )
url = 'https://dns.google.com/resolve'
headers = {
    'accept' : 'application/dns-json'
}

def get_json(url, headers):
        req = requests.get(
                url+query,
                headers=headers,
                verify=True
        )
        return(req.json())


def format_message(json):
    res = json
    try:
        HN = res['Answer'][0]['name']
        IP = res['Answer'][0]['data']
        message = "Name: %s has the IP: %s" % ( HN, IP )
        return(message)
    except KeyError:
        return("The host %s has no %s record" % ( NAME, TYPE ))

print(format_message(get_json(url, headers)))



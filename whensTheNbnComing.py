#!/usr/bin/env python

import json, requests

LOC = "LOC000030706211" 
url = 'https://places.nbnco.net.au/places/v2/details/%s' % LOC
headers = {
    'accept' : '*/*',
    'content_type' : 'json',
    'X-NBN-Breadcrumb-Id' : '1538202437980-418',
    'Referer' : 'https://www.nbnco.com.au/connect-home-or-business/check-your-address.html'
}

def get_json(url, headers):
        req = requests.get(
                url,
                headers=headers,
                verify=False
        )
        return(req.json())


def format_message(json):
    res = json
    approx = res['addressDetail']['rfsMessage']
    tech = res['addressDetail']['techType']
    status = res['addressDetail']['serviceStatus']
    message = "The NBN will arrive between: %s, \n Using technology: %s, \n Current Status is: %s" %(approx,tech,status)
    return(message)

print(format_message(get_json(url, headers)))



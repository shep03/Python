#!/usr/bin/env python

import json, requests


tp_list = {
   "Kleenex 9 pack": "679037",
   "Sorbent": "752108"
}


choice = input("Which TP do you want: %s?" %(tp_list))
if "Kleenex 9 pack" or "679037" in choice:
   item = "679037"
elif "Sorbent" or "785118" in choice:
   item = "752108"

postcode = input("Enter your postcode: ")
url = 'https://www.woolworths.com.au/apis/ui/product/%s/Stores?IncludeInStockStoreOnly=false&Max=5&postcode=%s' % (item, postcode)
#headers = {
#    'accept' : '*/*',
#    'content_type' : 'json',
#}

def get_json(url):
        req = requests.get(
                url,
                verify=True
        )
        return(req.json())


def format_message(json):
    for index in json:
      store = index['Store']['Name']
      price = index['InstoreListPrice']
      available = index['IsAvailable']
      if available:
         print("The item at %s, availability is %s and the price is %s" % (store, available, price))
      else:
         print("No TP at %s" % (store))

      #return(message)

format_message(get_json(url))
